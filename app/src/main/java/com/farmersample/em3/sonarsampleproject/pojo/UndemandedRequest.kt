package com.farmersample.em3.sonarsampleproject.pojo
import com.google.gson.annotations.SerializedName


data class UndemandedRequest(
    @SerializedName("amount")
    val amount: String,
    @SerializedName("distance")
    val distance: String,
    @SerializedName("franchise")
    val franchise: String,
    @SerializedName("intend_no")
    val intendNo: String,
    @SerializedName("start_date")
    val startDate: String,
    @SerializedName("vehicle")
    val vehicle: String
)