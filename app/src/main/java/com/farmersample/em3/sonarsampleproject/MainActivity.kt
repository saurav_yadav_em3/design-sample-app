package com.farmersample.em3.sonarsampleproject

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val items =  arrayListOf("List", "Generic Dialog", "Confirmation Dialog", "Generic widgets")
        designOptionsList.adapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, items)
        designOptionsList.setOnItemClickListener { parent, view, position, id ->
            when(position){
                0->{
                    startActivity(Intent(this, TypeOfListActivity::class.java))
                }
                1->{
                    AlertDialog.Builder(this, R.style.genericDialogTheme).setView(R.layout.generic_dialog).show()
                }
                2->{
                    AlertDialog.Builder(this).setView(R.layout.confirmation_dialog).show()
                }
                3->{
                    startActivity(Intent(this, GenericWidgetsActivity::class.java))
                }
            }
        }
    }
}
