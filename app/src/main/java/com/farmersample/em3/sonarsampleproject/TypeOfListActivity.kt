package com.farmersample.em3.sonarsampleproject

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.activity_type_of_list.*

class TypeOfListActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_type_of_list)

        val items =  arrayListOf("Basic List", "Complex list")
        ListOptions.adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, items)
        ListOptions.setOnItemClickListener { parent, view, position, id ->
            when(position){
                0->{

                }
                1->{
                    startActivity(Intent(this, ComplexListActivity1::class.java))
                }
            }
        }
    }
}